<?php
require_once dirname(__FILE__) . "/.config.inc.php";
error_reporting(E_ALL);
require_once "db/AppManager.php";
$db = AppManager::getPM();
$sql = "SELECT * FROM products WHERE is_discountinued=1 AND amazon_listed= 1 LIMIT 1";
$listedProducts = $db->fetchResult($sql);
if (empty($listedProducts)) {
    echo "Products not found for delete";
    die;
}
$products_ids = "";
$productIds = array();
$sku = array();
foreach ($listedProducts as $deleteing_product) {
    if(!empty($deleteing_product['product_number'])){
        $response = deleteAmazonProducts($deleteing_product['product_number']);
        $submit_response = invokeSubmitFeed($response['service'], $response['request']);
        print_r($submit_response);
    }
   
}



function deleteAmazonProducts($sku)
{
    $feed = deleteProduct($sku);
    $config = array(
        'ServiceURL' => SERVICE_URL,
        'ProxyHost' => null,
        'ProxyPort' => -1,
        'MaxErrorRetry' => 3,
    );
    $service = new MarketplaceWebService_Client(
        AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, $config, APPLICATION_NAME, APPLICATION_VERSION);
    $feed = file_get_contents($localFile);
    $feedHandle = @fopen('php://temp', 'rw+');
    fwrite($feedHandle, $feed);
    rewind($feedHandle);
    $request = new MarketplaceWebService_Model_SubmitFeedRequest();
    $request->setMerchant(MERCHANT_ID);
    $request->setMarketplace(MARKETPLACE_ID);
    $request->setFeedType($type);
    $request->setContentMd5(base64_encode(md5(stream_get_contents($feedHandle), true)));
    rewind($feedHandle);
    $request->setFeedContent($feedHandle);
    $request->setMWSAuthToken(MWSAuthToken);
    return array('service' => $service, 'request' => $request);
}

function invokeSubmitFeed(MarketplaceWebService_Interface $service, $request)
{
    try {
        $response = $service->submitFeed($request);
        if ($response->isSetSubmitFeedResult()) {
            $submitFeedResult = $response->getSubmitFeedResult();
            if ($submitFeedResult->isSetFeedSubmissionInfo()) {
                $feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
                if ($feedSubmissionInfo->isSetFeedSubmissionId()) {
                    echo ("Feed submited successfully : " . $feedSubmissionInfo->getFeedSubmissionId() . "\n");
                }
                if ($feedSubmissionInfo->isSetFeedType()) {

                }
                if ($feedSubmissionInfo->isSetSubmittedDate()) {

                }
                if ($feedSubmissionInfo->isSetFeedProcessingStatus()) {

                }
                if ($feedSubmissionInfo->isSetStartedProcessingDate()) {

                }
                if ($feedSubmissionInfo->isSetCompletedProcessingDate()) {

                }
            }
        }
    } catch (MarketplaceWebService_Exception $ex) {
        echo ("Caught Exception: " . $ex->getMessage() . "\n");
        echo ("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo ("Error Code: " . $ex->getErrorCode() . "\n");
        echo ("Error Type: " . $ex->getErrorType() . "\n");
        echo ("Request ID: " . $ex->getRequestId() . "\n");
        echo ("XML: " . $ex->getXML() . "\n");
    }
}

 function deleteProduct($sku) {
    return '<?xml version="1.0"?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
<Header>
<DocumentVersion>1.01</DocumentVersion>
<MerchantIdentifier>' . MERCHANT_ID . '</MerchantIdentifier>
</Header>
<MessageType>Product</MessageType>
<Message>
<MessageID>1</MessageID>
<OperationType>Delete</OperationType>
<Product>
<SKU>' . $sku . '</SKU>
</Product>
</Message>
</AmazonEnvelope>';
}