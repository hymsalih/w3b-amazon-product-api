<?php

set_time_limit(0);
const key_code = "AC92AE76655748D1A3A2768D09";

class AppManager {

    private static $pm;

    public static function getPM() {
        if (self::$pm === null) {
            include_once 'PersistanceManager.php';
            self::$pm = new PersistanceManager();
        }
        return self::$pm;
    }

}

?>