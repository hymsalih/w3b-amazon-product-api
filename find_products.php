<?php
error_reporting(E_ALL);
require_once "db/AppManager.php";
$db = AppManager::getPM();
$serviceUrl = "https://mws.amazonservices.com/Products/2011-10-01";
require_once dirname(__FILE__) . "/.config.inc.php";
$config = array(
    'ServiceURL' => $serviceUrl,
    'ProxyHost' => null,
    'ProxyPort' => -1,
    'ProxyUsername' => null,
    'ProxyPassword' => null,
    'MaxErrorRetry' => 3,
);
$products = $db->fetchResult("SELECT * FROM `products` WHERE `upc` != '' AND `amazon_processed` = 0");
//$products = $db->fetchResult("SELECT * FROM `products` WHERE `upc` != ''");
foreach($products  as $product){
    $upcLists =$product['upc'];
    $service = new MarketplaceWebServiceProducts_Client(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, APPLICATION_NAME, APPLICATION_VERSION, $config);
    $request = new MarketplaceWebServiceProducts_Model_GetMatchingProductForIdRequest();
    $request->setSellerId(MERCHANT_ID);
    $request->setMarketplaceId(MARKETPLACE_ID);
    $request->setMWSAuthToken(MWSAuthToken);
    $idListObj = new MarketplaceWebServiceProducts_Model_IdListType();
    $idListObj->setId($upcLists);
    $request->setIdList($idListObj);
    $request->setIdType('UPC');
    $response = invokeGetMatchingProductForId($service, $request);
    if (!empty($response) && isset($response->GetMatchingProductForIdResult)) {
        $matching_results = $response->GetMatchingProductForIdResult;
        foreach ($matching_results as $matching_result) {
            $matching_upc = (string) $matching_result->attributes()->Id;
            $matching_status = (string) $matching_result->attributes()->status;
            if ($matching_status == "Success") {
                $matched_product_asin = (string) $matching_result->Products->Product->Identifiers->MarketplaceASIN->ASIN;
                $sql = "UPDATE  `products`  SET  `amazon_processed`=1 , `asin`='$matched_product_asin'  WHERE upc='$matching_upc' ";
                $db->executeQuery($sql);
            } else {
                $sql = "UPDATE  `products`  SET  `amazon_processed`=1 WHERE upc='$matching_upc' ";
                $db->executeQuery($sql);
            }
        }
    
    }else{
    print_r($response );
    die;
    }
}

function invokeGetMatchingProductForId(MarketplaceWebServiceProducts_Interface $service, $request)
{
    try {
        $response = $service->GetMatchingProductForId($request);
        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->saveXML();
        $xml = str_replace("ns2:", "", $dom->saveXML());
        $res = simplexml_load_string($xml);
        return $res;
    } catch (MarketplaceWebServiceProducts_Exception $ex) {
        print_r($ex);
        return "";
    }
}
