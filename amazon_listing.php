<?php

require_once dirname(__FILE__) . "/.config.inc.php";
error_reporting(E_ALL);
require_once "db/AppManager.php";
$db = AppManager::getPM();
$sql = "SELECT products.*, price_lists.price,inventory_lists.quantity_available FROM products
LEFT JOIN price_lists ON products.id = price_lists.product_number
LEFT JOIN inventory_lists ON products.id = inventory_lists.product_number
WHERE products.asin !='' AND products.amazon_listed=0 AND products.is_discountinued=0 AND inventory_lists.quantity_available>0 LIMIT 1000";
$matched_product_array = $db->fetchResult($sql);
if (empty($matched_product_array)) {
    echo "No matching products to list";
    die;
}
$products_ids = "";
foreach ($matched_product_array as $listing_product) {
    $productIds[] = $listing_product['id'];
    $sku = $listing_product['product_number'];
    $listing_title = $listing_product['name'];
    $listing_description = $listing_product['description'];
    $condition_type = 11;
    $asin = $listing_product['asin'];
    $quantity = $listing_product['quantity_available'];
    $price = $listing_product['price'] * 1.70;
    $data[] = array($sku, $price, $quantity, $asin, "ASIN", $condition_type, "", "", $listing_title, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    $products_ids .= $listing_product['id'] . ",";
}
$productsIds = rtrim($products_ids, ",");
$file_name = "listing_flat_file_.txt";
$outfile = "templates/" . $file_name;
$filename = "templates/Flat.File.Listingloader.txt";
$contents = file_get_contents($filename);
foreach ($data as $productData) {
    $contents .= "\n" . implode("\t", $productData);
}
file_put_contents($outfile, $contents);

$file_name = "templates/listing_flat_file_.txt";
$response = flatFilesUpload($file_name, $type = "_POST_FLAT_FILE_LISTINGS_DATA_");
$submit_response = invokeSubmitFeed($response['service'], $response['request'], $productsIds);
print_r($submit_response);
die;

function flatFilesUpload($localFile, $type)
{
    if (!file_exists($localFile)) {
        return 'File does not exist!';
    }
    $config = array(
        'ServiceURL' => SERVICE_URL,
        'ProxyHost' => null,
        'ProxyPort' => -1,
        'MaxErrorRetry' => 3,
    );
    $service = new MarketplaceWebService_Client(
        AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, $config, APPLICATION_NAME, APPLICATION_VERSION);
    $feed = file_get_contents($localFile);
    $feedHandle = @fopen('php://temp', 'rw+');
    fwrite($feedHandle, $feed);
    rewind($feedHandle);
    $request = new MarketplaceWebService_Model_SubmitFeedRequest();
    $request->setMerchant(MERCHANT_ID);
    $request->setMarketplace(MARKETPLACE_ID);
    $request->setFeedType($type);
    $request->setContentMd5(base64_encode(md5(stream_get_contents($feedHandle), true)));
    rewind($feedHandle);
    $request->setFeedContent($feedHandle);
    $request->setMWSAuthToken(MWSAuthToken);
    return array('service' => $service, 'request' => $request);
}

function invokeSubmitFeed(MarketplaceWebService_Interface $service, $request, $productsIds)
{
    global $db;
    try {
        $response = $service->submitFeed($request);
        if ($response->isSetSubmitFeedResult()) {
            $submitFeedResult = $response->getSubmitFeedResult();
            if ($submitFeedResult->isSetFeedSubmissionInfo()) {
                $feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
                if ($feedSubmissionInfo->isSetFeedSubmissionId()) {
                    echo ("Feed submited successfully : " . $feedSubmissionInfo->getFeedSubmissionId() . "\n");
                    $sql = "UPDATE products SET amazon_listed=1 WHERE id IN($productsIds)";
                    $db->executeQuery($sql);
                }
                if ($feedSubmissionInfo->isSetFeedType()) {

                }
                if ($feedSubmissionInfo->isSetSubmittedDate()) {

                }
                if ($feedSubmissionInfo->isSetFeedProcessingStatus()) {

                }
                if ($feedSubmissionInfo->isSetStartedProcessingDate()) {

                }
                if ($feedSubmissionInfo->isSetCompletedProcessingDate()) {

                }
            }
        }
    } catch (MarketplaceWebService_Exception $ex) {
        echo ("Caught Exception: " . $ex->getMessage() . "\n");
        echo ("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo ("Error Code: " . $ex->getErrorCode() . "\n");
        echo ("Error Type: " . $ex->getErrorType() . "\n");
        echo ("Request ID: " . $ex->getRequestId() . "\n");
        echo ("XML: " . $ex->getXML() . "\n");
    }
}
